References:
 - https://github.com/cmccarthyIrl/spring-cucumber-serenity-test-harness
 - https://github.com/serenity-bdd/serenity-core/#what-is-the-latest-stable-version-i-should-use

Tasks:
 - ./gradlew cucumber
 - ./gradlew clean build automationTest