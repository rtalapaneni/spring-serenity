package com.tgc.poc.springserenity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSerenityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringSerenityApplication.class, args);
	}

}
