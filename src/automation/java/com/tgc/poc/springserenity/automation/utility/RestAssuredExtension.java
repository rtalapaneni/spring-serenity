package com.tgc.poc.springserenity.automation.utility;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public class RestAssuredExtension {

    public static RequestSpecification RequestSpecification;

    public RestAssuredExtension() {
        RequestSpecBuilder builder = new RequestSpecBuilder();
        builder.setBaseUri("http://localhost:8080");
        RequestSpecification = RestAssured.given().spec(builder.build());
    }

    public static ResponseOptions<Response> PerformGetOperationWithQueryParameter(String url, Map<String, String> queryParams) {
        try {
            RequestSpecification.queryParams(queryParams);
            return RequestSpecification.get(new URI(url));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }
}
