package com.tgc.poc.springserenity.automation.steps;

import com.tgc.poc.springserenity.automation.utility.RestAssuredExtension;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.restassured.response.Response;
import io.restassured.response.ResponseOptions;

import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.equalTo;

public class GreetSteps {
    private static ResponseOptions<Response> Response;

    @Given("I perform GET Operation for {string}")
    public void iPerformGETOperationFor(String arg0) {
        Map<String,String> queryParams = new HashMap<>();
        queryParams.put("name", arg0);
        Response = RestAssuredExtension.PerformGetOperationWithQueryParameter("/greet", queryParams);
    }

    @Then("I should see greet message {string}")
    public void iShouldSeeGreetMessage(String arg0) {
        assertThat(Response.getBody().print(), equalTo(arg0));
    }
}
